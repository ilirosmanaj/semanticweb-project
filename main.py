#!/usr/bin/env python3

from openpyxl import load_workbook
from custom_turtle_converter import CustomTurtleConverter

DATASET_PATH = 'data/dataset-cleaned.xlsx'
TURTLE_FILE_PATH = 'data/rdf-schema.ttl'


def convert_xls_to_turtle():
    """
    Converts the given dataset to a turtle file
    :return:
    """
    ws = load_workbook(DATASET_PATH).active
    converter = CustomTurtleConverter()

    # start writing prefixes and object properties
    write_to_turtle_file(converter.get_prefixes())
    write_to_turtle_file(converter.get_object_properties())

    for row in range(2, ws.max_row):
        # start from the years
        for col in range(3, ws.max_column):
            write_to_turtle_file(
                converter.convert_xlsx_row_to_triple(ws[row][0].value, # residence country
                                                 ws[row][1].value, # origin country
                                                 ws[row][2].value, # population type
                                                 ws[1][col].value, # year
                                                 ws[row][col].value)) # no of moved
        print('Processed : {}'.format(row - 1))

        if row == 2000:
            break


def write_to_turtle_file(string_list: list):
    if string_list:
        with open(TURTLE_FILE_PATH, 'a') as fp:
            fp.write('\n')

            for item in string_list:
                fp.write(item + '\n')


def main():
    convert_xls_to_turtle()


if __name__ == '__main__':
    main()
