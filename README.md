# SemanticWeb-Project

Convert CSV dataset to RDF format (Turtle)

# Running

This project runs on python 3.6.4, but locally consumes a FAST service which runs on port 5000 and uses python 2.7

Use: 

```python main.py```
