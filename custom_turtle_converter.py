from match_countries import get_countries_uri
COUNTRY_BASE_URL = 'http://country.org/'
COUNTRY_BASE_TYPE = '<http://dbpedia.org/ontology/country>'

POPULATION_TYPE_BASE_URL = 'http://population-type.org/'

MOVEMENT_BASE_URL = '<http://example.com/owl/populationMovement#>'
MOVEMENT_HAS_PROPERTY_NAME = 'hasPopulationMovement'
MOVEMENT_ORIGIN_COUNTRY = 'hasOriginCountry'
MOVEMENT_POPULATION_TYPE = 'hasPopulationType'
MOVEMENT_YEAR = 'hasYear'
MOVEMENT_NUMBER_OF_MOVED_PEOPLE = 'hasMovedPeople'

TAB_SPACES = '\t\t\t\t'

PREFIXES = {
    'RDFS': {
        'key': 'rdfs',
        'value': '<http://www.w3.org/2000/01/rdf-schema#>'
    },
    'RDF': {
        'key': 'rdf',
        'value': '<http://www.w3.org/1999/02/22-rdf-syntax-ns#>'
    },
    'XSD': {
        'key': 'xsd',
        'value': '<http://www.w3.org/2001/XMLSchema#>'
    },
    'OWL': {
        'key': 'owl',
        'value': '<http://www.w3.org/2002/07/owl#>'
    },
    'VCARD': {
        'key': 'vcard',
        'value': '<http://www.w3.org/2006/vcard/ns#>'
    },
    'MOVEMENT': {
        'key': 'm',
        'value': '<http://example.com/owl/populationMovement#>'
    }
}

BASE_URI = '<http://vocab.informatik.tuwien.ac.at/VU184.729-2018/11770999/>'


class CustomTurtleConverter(object):
    def get_prefixes(self):
        res = list()
        res.append('@base {} .'.format(BASE_URI))

        for item in PREFIXES.keys():
            res.append('@prefix {}: {} . '.format(
                PREFIXES[item]['key'],
                PREFIXES[item]['value']
            ))
        return res

    def get_object_properties(self):
        res = list()

        # declare properties for movement object
        res.append('{}:{} rdf:type owl:ObjectProperty .'.format(
            PREFIXES['MOVEMENT']['key'],
            MOVEMENT_POPULATION_TYPE
        ))

        res.append('{}:{} rdf:type owl:ObjectProperty .'.format(
            PREFIXES['MOVEMENT']['key'],
            MOVEMENT_POPULATION_TYPE
        ))

        res.append('{}:{} rdf:type owl:ObjectProperty .'.format(
            PREFIXES['MOVEMENT']['key'],
            MOVEMENT_ORIGIN_COUNTRY
        ))

        res.append('{}:{} rdf:type owl:ObjectProperty .'.format(
            PREFIXES['MOVEMENT']['key'],
            MOVEMENT_YEAR
        ))

        res.append('{}:{} rdf:type owl:ObjectProperty .'.format(
            PREFIXES['MOVEMENT']['key'],
            MOVEMENT_NUMBER_OF_MOVED_PEOPLE
        ))

        return res

    def convert_xlsx_row_to_triple(self, residence_country, origin_country, population_type, year, no_of_moved):
        res = []

        # return if no movement for that row
        if no_of_moved == '' or no_of_moved is None or no_of_moved == '*':
            return res

        # remove white spaces
        residence_country = residence_country.replace(' ', '').replace('(', '').replace(')', '')
        origin_country = origin_country.replace(' ', '').replace('(', '').replace(')', '')
        population_type = population_type.replace(' ', '')

        residence_country_uri = get_countries_uri(residence_country)
        origin_country_uri = get_countries_uri(origin_country)

        # declare the type of residence country
        # e.g. :Austria a :country
        res.append('{} a {} . '.format(
                        residence_country_uri if residence_country_uri else residence_country,
                        COUNTRY_BASE_TYPE
                    ))

        # declare country-name as vcard:country-name
        res.append('{} {}:{} "{}"^^xsd:string . '.format(
            residence_country_uri if residence_country_uri else residence_country,
            PREFIXES['VCARD']['key'],
            'country-name',
            residence_country
        ))

        blank_node_name_for_movement = self.generate_movement_name(residence_country, origin_country, year)

        # declare movement property (maybe not the best name)
        # e.g. :Austria :hasPopulationMovement _: movement1_blank_node
        res.append('{} {}:{} _:{} . '.format(
            residence_country_uri if residence_country_uri else '"{}^^xsd:string"'.format(residence_country),
            PREFIXES['MOVEMENT']['key'],
            MOVEMENT_HAS_PROPERTY_NAME,
            blank_node_name_for_movement
        ))

        # declare the properties of movement (e.g year, origin country and no_of_moved)
        # e.g. : _ : movement1_blank_node : hasOriginCountry : Austria ;
        res.append('_:{} {}:{} {} ; '.format(
            blank_node_name_for_movement,
            PREFIXES['MOVEMENT']['key'],
            MOVEMENT_ORIGIN_COUNTRY,
            origin_country_uri if origin_country_uri else '"{}^^xsd:string"'.format(origin_country),
        ))

        # declare the population type for that movement
        #  :hasPopulationType :refugees ;
        res.append('{} {}:{} "{}"^^xsd:string ; '.format(
            TAB_SPACES,
            PREFIXES['MOVEMENT']['key'],
            MOVEMENT_POPULATION_TYPE,
            population_type
        ))

        # declare the year of movement
        # e.g. : :hasYear 2000
        res.append('{} {}:{} "{}"^^xsd:int ; '.format(
            TAB_SPACES,
            PREFIXES['MOVEMENT']['key'],
            MOVEMENT_YEAR,
            year
        ))

        # declare the number of moved people
        # e.g. : _:movement1_blank_node :movedPeople 210
        res.append('{} {}:{} "{}"^^xsd:int . '.format(
            TAB_SPACES,
            PREFIXES['MOVEMENT']['key'],
            MOVEMENT_NUMBER_OF_MOVED_PEOPLE,
            no_of_moved
        ))

        return res

    @staticmethod
    def generate_movement_name(residence_country, origin_country, year):
        return 'movement{}To{}{}'.format(
            residence_country,
            origin_country,
            year
        )