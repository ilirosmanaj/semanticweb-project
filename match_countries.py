import requests
FAST_WEB_SERVICE = 'http://0.0.0.0:5000/reconcile'


def get_countries_uri(country_name):
    res = requests.post('{}?query={}&type=/fast/geographic'.format(
                        FAST_WEB_SERVICE,
                        country_name))

    if res.status_code != 200 or len(res.json().get('result')) == 0:
        return None
    else:
        return '<{}>'.format(res.json().get('result')[0]['id'])
